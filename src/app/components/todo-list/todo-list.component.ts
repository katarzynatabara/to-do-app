import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  
  private list: Array<Task>; //lista tasków
 

  constructor() {
    this.list = [];  //inicjacja listy tasków
    //ładowanie zapisanej listy z pamięci przeglądarki
    let loadedList = JSON.parse(localStorage.getItem("task-list"));
    // ustawia zapisaną listę jeśli istnieje
    if(loadedList != null){
      this.list = loadedList;
    }  
   }


  ngOnInit() {
  }
    //zwraca otwarte lub zamknięte taski
  get openTasks(): Array<Task>{
    return this.list.filter((t)=>{
      return !t.closed;
    });
  }
  get closedTasks(): Array<Task>{
    return this.list.filter((t)=>{
      return t.closed;
    });
  }

    
  onFormSubmit(form){
    //nie pozwala dodawać pustych tasków
    if(form.value.title == "" || form.value.title == null){
      return;
    }
    this.addTodo(form.value.title); 
    form.reset();
    // console.log(this.list)
    //zapisywanie listy tasków w lokalnej pamięci przeglądarki
    localStorage.setItem("task-list",JSON.stringify(this.list));
  }

  addTodo(title){
    //dodawanie nowego obiektu taska
    this.list.push({
      id: Math.random().toString(36).substr(2, 9), 
      title: title,
      important: false,
      closed: false
    });
  }  
    //zamykanie tasków
  closeTask(task: Task){
    // let index = this.list.indexOf(task);
    // if (index > -1){
    //   this.list.splice(index, 1);
    // }
    task.closed = true;
    //zapisywanie listy tasków w lokalnej pamięci przeglądarki
    localStorage.setItem("task-list",JSON.stringify(this.list));
  }

  openTask(task: Task){
    task.closed = false;
    //zapisywanie listy tasków w lokalnej pamięci przeglądarki
    localStorage.setItem("task-list",JSON.stringify(this.list));
  }
  //ustaw stan taska na ważny
  setImportantTask(task){
    task.important = true;
    //zapisywanie listy tasków w lokalnej pamięci przeglądarki
    localStorage.setItem("task-list",JSON.stringify(this.list));
  }
  setNotImportantTask(task){
    task.important = false;
    //zapisywanie listy tasków w lokalnej pamięci przeglądarki
    localStorage.setItem("task-list",JSON.stringify(this.list));
  }
}


//interfejs taska
export interface Task{
  id: string;
  title: string;
  important: boolean;
  closed: boolean;
}